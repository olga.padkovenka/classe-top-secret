<?php

require './Manager/DBManager.php';
require './Model/Mission.php';



class MissionManager extends DBManager{
    public function getAll() {
        $result = [];


        $stmt = $this->getConnexion()->query('SELECT * FROM missions');

        while($row = $stmt->fetch()) {
            $mission = new Mission();
            $mission->setcodeMission($row['code_mission']);
            $mission->settitreMission($row['titre_mission']);

            $result[] = $mission;
        }

        return $result;

    }

   
    
}