<?php
require './manager/AgentManager.php';
require './manager/PaysManager.php';
require './manager/PossedeManager.php';
// require './manager/DBManager.php';
$newagentManager = new AgentManager();
$addagentManager = new AgentManager();
if (isset($_POST['code_agent'])) {
    $code=$_POST['code_agent'];
    $nom=$_POST['nom_agent'];
    $prenom=$_POST['prenom_agent'];
    $codepays=$_POST['code_pays'];
    $agentManager = $newagentManager->getidentAgent($code);
    $date=$_POST['date_naissance_agent'];
    $specialite=$_POST['libelle_specialite'];
    echo('test si agent existe');
    echo($_POST['code_pays']);

    if ($agentManager !=null){
        print_r($_COOKIE['existagent']);
    } else {
        $paysAgent = new PaysManager();
        $paysmanager = $paysAgent->getidentpays($codepays);
        var_dump($paysmanager);

        if ($paysmanager != null) {
            $agent = new Agent;
            echo($_POST['nom_agent']);
            $agent->setcodeAgent($_POST['code_agent']);
            $agent->setnomAgent($_POST['nom_agent']);
            $agent->setprenomAgent($_POST['prenom_agent']);
            $agent->setdateNaissanceAgent($_POST['date_naissance_agent']);
            $agent->setcodePays($_POST['code_pays']);
            $agent->setLibelleSpecialite($_POST['libelle_specialite']);
            $addagent = $addagentManager->addAgent($agent);
            var_dump($addagent);
        } else {
            print_r($_COOKIE['nonexistpays']);
        }
    }
};
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout </title>
</head>
<body>
<div class="container">
    <h1>Table Agent </h1>
    <form action="ajoutAgent.php" method="POST" enctype="multipart/form-data">

        <input class="ml-5 col-sm-6" id="code_agent" type="text" name="code_agent"
               placeholder="code nouvel agent"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="nom_agent" type="text" name="nom_agent"
               placeholder="nom nouvel agent"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="prenom_agent" type="text" name="prenom_agent"
               placeholder="prenom nouvel agent"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="date_naissance_agent" type="date" name="date_naissance_agent"
               placeholder="date de naissance"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="code_pays" type="text" name="code_pays" placeholder="code pays"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" id="libelle_specialite" type="text" name="libelle_specialite" placeholder="libelle_specialite"
               autocomplete="off" required>

        <button class="col-sm-3 btn btn-outline-primary" id="valider" type="submit">Ajouter</button>
    </form>
    <a class="dropdown-item ml2" href="affAgent.php" name="retour">Retour</a>
</div>
</body>
</html>
