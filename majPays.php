<?php
require './manager/PaysManager.php';
// require './manager/DBManager.php';
$newpaysManager = new PaysManager();
$majpaysManager = new PaysManager();
$connexion = new PDO("mysql:host=localhost;dbname=catalogue_top_secret;port=3306;charset=utf8mb4", "admin", "azerty");
$code= $_GET['code'];
$sql =  "SELECT * FROM pays WHERE code_pays = '$code'";
$stmt = $connexion->query($sql);
var_dump($sql);

$row = $stmt->fetch();

if ($row) {
    $nompays = $row['libelle_pays'];
}

if (isset($_POST['libelle_pays'])) {
    $pays = new Pays;
    echo($_POST['libelle_pays']);
    $pays->setCodePays($_POST['code_pays']);
    $pays->setLibellePays($_POST['libelle_pays']);
    $majpays = $majpaysManager->majpays($pays);
    header('Location: affPays.php');
};
if (isset($_POST['retour'])) {
    header('Location: affPays.php');
};
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier </title>
</head>
<body>
<div class="container">
    <h1>Table pays </h1>
    <form action="majPays.php" method="POST" enctype="multipart/form-data">

        <input class="ml-5 col-sm-6" type="text" name="code_pays"
               placeholder="code nouveau pays" value="<?=$code?>"
               autocomplete="off" required>
        <input class="ml-5 col-sm-6" type="text" name="libelle_pays"
               placeholder="libelle pays" value="<?=$nompays?>"
               autocomplete="off" required>
        <button class="col-sm-3 btn btn-outline-primary" href="affPays.php" name="retour" type="submit">Retour liste pays</button>
        <button class="col-sm-3 btn btn-outline-primary" id="valider" type="submit">Modifier</button>
    </form>
</div>
</body>
</html>
