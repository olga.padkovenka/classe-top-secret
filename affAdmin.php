<?php
    require './manager/AdminManager.php';

    $adminManager = new AdminManager();

    $admins = $adminManager->getAllAdmin();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage table Admin</title>
</head>
<body>
    <div class="container">
    <form action="affAdmin.php" method="POST" enctype="multipart/form-data">
        <div class="input-group mb-3">
         <?php
            foreach ($admins as $admin) {
            ?>
            <p href="#" id="<?= $admin->getcodeAdmin() ?>">
            <?= $admin->getcodeAdmin() . ' ' . $admin->getnomAdmin() . ' '.
            $admin->getprenomAdmin() . ' ' . $admin->getadresseEmailAdmin()
            . ' ' .  $admin->getdateCreatAdmin(); ?>
                <a class="dropdown-item ml2" href="majAdmin.php?code=<?= $admin->getcodeAdmin() ?>"
                   name="modif">Modification</a>
                <a class="dropdown-item ml2" href="suppAdmin.php?code=<?= $admin->getcodeAdmin() ?>"
                   name="suppr">Suppression</a>
            </p>
            <?php
                }
            ?>
        </div>
       <!--
         ?>
        -->

        <div class="dropdown mt2">
        <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Action choisie
        </button>  -->
             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item ml6" href="ajoutAdmin.php" name="ajout">Ajout</a>
                 <a class="dropdown-item ml2" href="actions_admin.php" name="retour">Retour</a>
             </div>
        </div>
    </form>
    </div>
</body>
</html>

