<?php
require './manager/SpecialiteManager.php';
// require './manager/DBManager.php';
$newspecialiteManager = new SpecialiteManager();
$addspecialiteManager = new SpecialiteManager();
if (isset($_POST['libelle_specialite'])) {
    $code=$_POST['libelle_specialite'];
    $specialiteManager = $newspecialiteManager->getidentspecialite($code);

    if ($specialiteManager != null){
        print_r($_COOKIE['existspecialite']);
    } else {
        $specialite= new specialite;

        $specialite->setLibellespecialite($_POST['libelle_specialite']);
        $ajspecialite = $addspecialiteManager->addspecialite($specialite);

    }
};
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout </title>
</head>
<body>
<div class="container">
    <h1>Table specialite </h1>
    <form action="ajoutSpecialite.php" method="POST" enctype="multipart/form-data">

        <input class="ml-5 col-sm-6" type="text" name="libelle_specialite"
               placeholder="libelle specialite"
               autocomplete="off" required>

        <button class="col-sm-3 btn btn-outline-primary" id="valider" type="submit">Ajouter</button>
    </form>
    <a class="dropdown-item ml2" href="affSpecialite.php" name="retour">Retour</a>
</div>
</body>
</html>
