<?php
    require_once './manager/AgentManager.php';
    require_once './manager/PossedeManager.php';
    $agentManager = new AgentManager();
    $agents = $agentManager->getAllAgent();

    setcookie('existagent','cet agent existe déjà');
    setcookie('nonexistpays','ce pays est inexistant');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage table Agent</title>
</head>
<body>
    <div class="container">
    <form action="affAgent.php" method="POST" enctype="multipart/form-data">
        <div class="input-group mb-3">
         <?php
            foreach ($agents as $agent) {
              ?>
            <p href="#" id="<?= $agent->getcodeAgent() ?>">
            <?= $agent->getcodeAgent() . ' ' . $agent->getnomAgent() . ' '.
                $agent->getprenomAgent() . ' ' . $agent->getdateNaissanceAgent()
            . ' ' .  $agent->getcodePays() . ' ' . $agent->getLibelleSpecialite(); ?>
                <a class="dropdown-item ml2" href="majAgent.php?code=<?= $agent->getcodeAgent() ?>"
                   name="modif">Modification</a>
                <a class="dropdown-item ml2" href="suppAgent.php?code=<?= $agent->getcodeAgent() ?>"
                   name="suppr">Suppression</a>
            </p>
            <?php
                }
            ?>
        </div>
       <!--
         ?>
        -->

        <div class="dropdown mt2">
        <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Action choisie
        </button>  -->
             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item ml2" href="ajoutAgent.php" name="ajout">Ajout</a>
                 <a class="dropdown-item ml2" href="actions_admin.php" name="retour">Retour</a>
             </div>
        </div>
    </form>
    </div>
</body>
</html>
