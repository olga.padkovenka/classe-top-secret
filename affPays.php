<?php
    require './manager/PaysManager.php';

    $paysManager = new paysManager();

    $tspays = $paysManager->getAllpays();
    setcookie('existpays','cet pays existe déjà');
    setcookie('nonexistpays','ce pays est inexistant');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage table pays</title>
</head>
<body>
    <div class="container">
    <form action="affPays.php" method="POST" enctype="multipart/form-data">
        <div class="input-group mb-3">
         <?php
            foreach ($tspays as $pays) {
            ?>
            <p href="#" id="<?= $pays->getCodePays() ?>">
            <?= $pays->getCodePays() . ' ' . $pays->getLibellePays(); ?>
                <a class="dropdown-item ml2" href="majPays.php?code=<?= $pays->getCodePays() ?>"
                   name="modif">Modification</a>
                <a class="dropdown-item ml2" href="suppPays.php?code=<?= $pays->getCodePays() ?>"
                   name="suppr">Suppression</a>
            </p>
            <?php
                }
            ?>
        </div>


        <div class="dropdown mt2">
        <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Action choisie
        </button>  -->
             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item ml2" href="ajoutPays.php" name="ajout">Ajout</a>
                 <a class="dropdown-item ml2" href="actions_admin.php" name="retour">Retour</a>
             </div>
        </div>
    </form>
    </div>
</body>
</html>
