<?php


class Pays
{
private $code_pays;
private $libelle_pays;
    /**
     * @return mixed
     */
    public function getCodePays()
    {
        return $this->code_pays;
    }

    /**
     * @param mixed $code_pays
     */
    public function setCodePays($code_pays): void
    {
        $this->code_pays = $code_pays;
    }

    /**
     * @return mixed
     */
    public function getLibellePays()
    {
        return $this->libelle_pays;
    }

    /**
     * @param mixed $libelle_pays
     */
    public function setLibellePays($libelle_pays): void
    {
        $this->libelle_pays = $libelle_pays;
    }


}
