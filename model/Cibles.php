<?php

class Cibles {
    private $codeCible;
    private $nomCible; 
    private $prenomCible;
    private $dateNaissanceCible;
    private $codePays;
    private $codeMission;
    
    public function getcodeCible() {
        return $this->code_cible;
    }

    public function setcodeCible($codeCible) {
        $this->code_cible = $codeCible;
    }
    public function getnomCible() {
        return $this->nom_cible;
    }

    public function setnomCible($nomCible) {
        $this->nom_cible = $nomCible;
    }
    public function getprenomCible() {
        return $this->prenom_cible;
    }

    public function setprenomCible($prenomCible) {
        $this->prenom_cible = $prenomCible;
    }
    
    public function getdateNaissanceCible() {
        return $this->date_naissance_cible;
    }

    public function setdateNaissanceCible($dateNaissanceCible) {
        $this->date_naissance_cible = $dateNaissanceCible;
    }
   
    public function getcodePays() {
        return $this->code_pays;
    }
    public function setcodePays($codePays) {
        $this->code_pays = $codePays;
    }
    public function getcodeMission() {
        return $this->code_mission;
    }
    public function setcodeMission($codeMission) {
        $this->code_mission = $codeMission;
    }   
}