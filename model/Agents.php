<?php

class Agent {

    private $code_agent;
    private $nom_agent;
    private $prenom_agent;
    private $date_naissance_agent;
    private $code_pays;
    private $libelle_specialite;

    /**
     * @return mixed
     */
    public function getLibelleSpecialite()
    {
        return $this->libelle_specialite;
    }

    /**
     * @param mixed $libelle_specialite
     */
    public function setLibelleSpecialite($libelle_specialite): void
    {
        $this->libelle_specialite = $libelle_specialite;
    }

    public function getcodeAgent() {
        return $this->code_agent;
    }
    public function setcodeAgent($code_agent) {
        $this->code_agent = $code_agent;
    }
    public function setnomAgent($nom_agent) {
        $this->nom_agent = $nom_agent;
    }
    public function getnomAgent() {
        return $this->nom_agent;
    }
    public function setprenomAgent($prenom_agent) {
        $this->prenom_agent = $prenom_agent;
    }
    public function getprenomAgent() {
        return $this->prenom_agent;
    }
    public function setdateNaissanceAgent($date_naissance_agent) {
        $this->date_naissance_agent = $date_naissance_agent;
    }

    public function getdateNaissanceAgent() {
        return $this->date_naissance_agent;
    }

    public function getcodePays() {
        return $this->code_pays;
    }
    public function setcodePays($code_pays) {
        $this->code_pays = $code_pays;

    }

}
