<?php

class Mission {
    private $codeMission; 
    private $tiTre;
    private $desCription;
    private $dateDebut;
    private $dateFin;
    private $codePays;
    private $libelleMission;
    private $libelleStatut;

    public function getcodeMission() {
        return $this->code_mission;
    }

    public function setcodeMission($codeMission) {
        $this->code_mission = $codeMission;
    }
    public function gettiTre() {
        return $this->titre;
    }

    public function settiTre($tiTre) {
        $this->titre = $tiTre;
    }

    public function getdesCription() {
        return $this->description;
    }

    public function setdesCription($desCription) {
        $this->description = $desCription;
    }
    public function getdateDebut() {
        return $this->date_debut;
    }

    public function setdateDebut($dateDebut) {
        $this->date_debut = $dateDebut;
    }
    public function getdateFin() {
        return $this->date_fin;
    }

    public function setdateFin($dateFin) {
        $this->date_fin = $dateFin;
    }
    
    public function getcodePays() {
        return $this->code_pays;
    }
    public function setcodePays($codePays) {
        $this->code_pays = $codePays;
    }
    public function getlibelleMission() {
        return $this->libelle_mission;
    }
    public function setlibelleMission($libelleMission) {
        $this->libelle_mission = $libelleMission;
    }
    public function getlibelleStatut() {
        return $this->libelle_statut;
    }
    public function setlibelleStatut($libelleStatut) {
        $this->libelle_statut = $libelleStatut;
    }
}