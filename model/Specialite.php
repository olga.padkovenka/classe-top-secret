<?php


class Specialite
{
private $libelle_specialite;

    /**
     * @return mixed
     */
    public function getLibelleSpecialite()
    {
        return $this->libelle_specialite;
    }

    /**
     * @param mixed $libelle_specialite
     */
    public function setLibelleSpecialite($libelle_specialite): void
    {
        $this->libelle_specialite = $libelle_specialite;
    }
}
