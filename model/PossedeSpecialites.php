<?php


class Possede_specialites
{
    private $codeAgent;

    /**
     * @return mixed
     */
    public function getCodeAgent()
    {
        return $this->codeAgent;
    }

    /**
     * @param mixed $codeAgent
     */
    public function setCodeAgent($codeAgent): void
    {
        $this->codeAgent = $codeAgent;
    }

    /**
     * @return mixed
     */
    public function getLibelleSpecialite()
    {
        return $this->libelleSpecialite;
    }

    /**
     * @param mixed $libelleSpecialite
     */
    public function setLibelleSpecialite($libelleSpecialite): void
    {
        $this->libelleSpecialite = $libelleSpecialite;
    }
    private $libelleSpecialite;

}
