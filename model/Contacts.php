<?php

class Contacts {
   
     private $codeContact;
     private $nomContact;
     private $prenomContact;
     private $dateNaissanceContact;
     private $codePays;
    
    public function getcodeContact() {
        return $this->code_contact;
    }

    public function setcodeContact($codeContact) {
        $this->code_contact = $codeContact;
    }
    public function getnomContact() {
        return $this->nom_contact;
    }

    public function setnomContact($nomContact) {
        $this->nom_contact = $nomContact;
    }
      public function getprenomContact() {
        return $this->prenom_contact;
    }

    public function setprenomContact($prenomContact) {
        $this->prenom_contact = $prenomContact;
    }

    public function getdateNaissanceContact() {
        return $this->date_naissance_contact;
    }

    public function setdateNaissanceContact($dateNaissanceContact) {
        $this->date_naissance_contact = $dateNaissanceContact;
    }
 
    public function getcodePays() {
        return $this->code_pays;
    }
    public function setcodePays($codePays) {
        $this->code_pays = $codePays;
    }
  }