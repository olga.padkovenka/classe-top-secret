<?php

class Admin {
    private $code_admin;
    private $nom_admin;
    private $prenom_admin;
    private $adresse_email_admin;
    private $pwd_admin;
    private $date_creat_admin;

    public function getcodeAdmin() {
        return $this->code_admin;
    }

    public function setcodeAdmin($code_admin) {
        $this->code_admin = $code_admin;
    }
    public function getnomAdmin() {
        return $this->nom_admin;
    }

    public function setnomAdmin($nom_admin) {
        $this->nom_admin = $nom_admin;
    }

    public function getprenomAdmin() {
        return $this->prenom_admin;
    }

    public function setprenomAdmin($prenom_admin) {
        $this->prenom_admin = $prenom_admin;
    }

    public function getadresseEmailAdmin() {
        return $this->adresse_email_admin;
    }

    public function setadresseEmailAdmin($adresse_email_admin) {
        $this->adresse_email_admin = $adresse_email_admin;
    }
    public function getdateCreatAdmin() {
        return $this->date_creat_admin;
    }

    public function setdateCreatAdmin($date_creat_admin) {
        $this->date_creat_admin = $date_creat_admin;
    }

    public function setpwdAdmin($pwd_admin) {
        $this->pwd_admin = $pwd_admin;
    }
    public function getpwdAdmin() {
        return $this->pwd_admin;
    }

}
