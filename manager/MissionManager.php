<?php

require './Manager/DBManager.php';
require './Model/Missions.php';

 
class MissionsManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM Missions');

        while($row = $stmt->fetch()) {
            $mission = new Mission();
            $mission->setcodeMission($row['code_mission']);
            $mission->setdesCription($row['description']);
            $mission->settiTre($row['titre']);
            $mission->setdateDebut($row['date_debut']);
            $mission->setdateFin($row['date_fin']);
            $mission->setcodePays($row['code_pays']);
            $mission->setlibelleMission($row['libelle_mission']);
            $mission->setlibelleStatut($row['libelle_statut']);
           
            $result[] = $mission;
        }

        return $result;
    }

    public function addMission($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions 
         VALUES (code_mission = :code, titre =  :titre, description = :description, 
         date_debut = :date_debut, date_fin = :date_fin, code_pays = :code_pays,
         libelle_mission = :libelle_mission, libelle_statut = : libelle_statut)');
        $stmt->execute(['code' => $mission->getcodeMission(),
        'titre' => $mission->gettiTre(),
        'description' => $mission->getdesCription(),
        'date_debut' => $mission->getdateDebut(),
        'date_fin'=> $mission->getdateFin(),
        'code_pays'=> $mission->getcodePays(),
        'libelle_mission'=> $mission->getlibelleMission(),
        'libelle_statut'=> $mission->getlibelleStatut()
        ]);
        return true;
    }
}