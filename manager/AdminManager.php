<?php

require './manager/DBManager.php';
require './model/Admins.php';

class AdminManager extends DBManager{
    public function getAllAdmin() {
        $result = [];

        $stmt = $this->getConnexion()->query
        ('SELECT * FROM admins');

        while($row = $stmt->fetch()) {
            $admin = new Admin();
            $admin->setcodeAdmin($row['code_admin']);
            $admin->setnomAdmin($row['nom_admin']);
            $admin->setprenomAdmin($row['prenom_admin']);
            $admin->setadresseEmailAdmin($row['adresse_email_admin']);
            $admin->setpwdAdmin($row['pwd_admin']);
            $admin->setdateCreatAdmin($row['date_creat_admin']);
            $result[] = $admin;
        }
        return $result;
    }
    public function getidentAdmin($adresse_email_admin, $pwd_admin) {
        $admin = null;
        $stmt = $this->getConnexion()->prepare('SELECT * FROM admins
         WHERE adresse_email_admin = :adresse_email_admin
         AND pwd_admin = :pwd_admin');
        $stmt->execute(['adresse_email_admin' => $adresse_email_admin,
         'pwd_admin'=> $pwd_admin]);
        $row = $stmt->fetch();

        if ($row) {

            $admin = new Admin();
            $admin->setcodeAdmin($row['code_admin']);
            $admin->setnomAdmin($row['nom_admin']);
            $admin->setprenomAdmin($row['prenom_admin']);
            $admin->setadresseEmailAdmin($row['adresse_email_admin']);
            $admin->setpwdAdmin($row['pwd_admin']);
            $admin->setdateCreatAdmin($row['date_creat_admin']);
        };
        return $admin;
    }
    public function addAdmin(Admin $admin) {

        $code = $admin->getcodeAdmin();
        $nom = $admin->getnomAdmin();
        $prenom = $admin->getprenomAdmin();
        $email = $admin->getadresseEmailAdmin();
        $pwd = $admin->getpwdAdmin();
        $date = $admin->getdateCreatAdmin();

        $sql = "INSERT INTO admins 
         VALUES ('$code', '$nom', '$prenom', '$email', '$pwd', '$date')";

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;


       }
    public function majAdmin(Admin $admin) {

        $code = $admin->getcodeAdmin();
        $nom = $admin->getnomAdmin();
        $prenom = $admin->getprenomAdmin();
        $email = $admin->getadresseEmailAdmin();
        $pwd = $admin->getpwdAdmin();
        $date = $admin->getdateCreatAdmin();

        $sql = "UPDATE admins 
        set nom_admin = '$nom',
            prenom_admin = '$prenom',
            adresse_email_admin = '$email',
            pwd_admin = '$pwd',
            date_creat_admin = '$date'
            WHERE code_admin = '$code'";

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;

    }
    public function supAdmin($code) {

        $sql = "DELETE FROM admins 
        WHERE code_admin = '$code'";

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;

    }
}
