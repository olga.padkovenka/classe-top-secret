<?php
require_once './manager/DBManager.php';
require_once './model/Pays.php';

class PaysManager extends DBManager
{
    public function getAllPays() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM pays');

        while($row = $stmt->fetch()) {
            $pays = new Pays();
            $pays->setCodePays($row['code_pays']);
            $pays->setLibellePays($row['libelle_pays']);
            $result[] = $pays;
        }

        return $result;
    }
    public function getidentpays($code_pays) {
        $pays = null;
        $stmt = $this->getConnexion()->prepare('SELECT * FROM pays
         WHERE code_pays = :code_pays');
        $stmt->execute(['code_pays' => $code_pays]);
        $row = $stmt->fetch();
        var_dump($stmt);
        if ($row) {
            $pays = new Pays();
            $pays->setCodePays($row['code_pays']);
            $pays->setLibellePays($row['libelle_pays']);
        };
        return $pays;
    }
    public function addpays(Pays $pays) {

        $code = $pays->getCodePays();
        $nompays = $pays->getLibellePays();

        $sql = "INSERT INTO pays VALUES ('$code', '$nompays')";

        var_dump($sql);

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;
    }

    public function majpays(Pays $pays) {
        $code = $pays->getCodePays();
        $nompays = $pays->getLibellePays();

        $sql = "UPDATE pays
        set libelle_pays = '$nompays'
            WHERE code_pays = '$code'";
        var_dump($sql);

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;

    }
    public function suppays($code) {

        $sql = "DELETE FROM pays 
        WHERE code_pays = '$code'";

        $stmt = $this->getConnexion()->query($sql);

        return $stmt;

    }

}
